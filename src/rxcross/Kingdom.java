package rxcross;

import java.util.ArrayList;
import java.util.List;

// Contains a pool of candidates
public class Kingdom implements Runnable {
    Object lock = new Object();
    List<Candidate> candidates = new ArrayList<>();

    int id;

    Candidate gold;
    int goldMedal;
    Candidate silver;
    int silverMedal;
    boolean terminated = false;

    Kingdom(int id) {
        this.id = id;

        for (int i = 0; i < 100; i++) {
            candidates.add(Candidate.randomCandidate());
            gold = Candidate.randomCandidate();
            silver = Candidate.randomCandidate();
        }
    }

    public synchronized void marryInto(Kingdom kingdom) {
        synchronized (lock) {
            // Let's have 10 kids
            for (int i = 0; i < 10; i++) {
                candidates.add(gold.breedWith(kingdom.gold));
            }
        }
    }

    @Override
    public void run() {
        while (!terminated && goldMedal < 36) {
            synchronized (lock) {
                for (Candidate candidate : candidates) {
                    int score = candidate.evaluate();
                    if (score >= goldMedal) {
                        silverMedal = goldMedal;
                        goldMedal = score;
                        gold = candidate;
                    } else if (score >= silverMedal) {
                        silverMedal = score;
                        silver = candidate;
                    }
                }

                candidates.clear();

                for (int i = 0; i < 5; i++) {
                    candidates.add(gold.breedWith(silver));
                }
                for (int i = 0; i < 85; i++) {
                    candidates.add(gold.breedWith(silver).mutate());
                }
                for (int i = 0; i < 10; i++) {
                    candidates.add(Candidate.randomCandidate());
                }
            }
        }
        if (!terminated) {
            System.out.println("----- FOUND ONE! ------");
            gold.print();
            System.out.println("Final score: " + Integer.toString(gold.evaluate()));
            System.exit(0);
        }
    }
}
