package rxcross;

import java.util.Properties;
import java.util.Random;

public class Candidate {
    char[][] dna;

    final static char[] characterSet = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
    final static Random r = new Random();

    static Candidate randomCandidate() {
        Candidate c = new Candidate();
        c.dna = new char[14][14];
        for (int x = 0; x <= 13; x++) {
            for (int y = 0; y <= 13; y++) {
                c.dna[x][y] = characterSet[r.nextInt(26)];
            }
        }
        return c;
    }

    Candidate() {
        // Empty constructor is fine
    }

    // Load a candidate from a properties file
    Candidate(Properties p) {
        dna = new char[14][14];
        for (int y = 12; y >= 0; y--) {
            for (int x = 12; x >= 0; x--) {
                if (r.nextInt(100) <= 10) {
                    dna[x][y] = p.getProperty("dna." + Integer.toString(x) + "." + Integer.toString(y)).charAt(1);
                }
            }
        }
    }

    // Save a candidate to a properties file
    void saveTo(Properties p) {
        for (int y = 12; y >= 0; y--) {
            for (int x = 12; x >= 0; x--) {
                if (r.nextInt(100) <= 10) {
                    p.setProperty("dna." + Integer.toString(x) + "." + Integer.toString(y), String.valueOf(dna[x][y]));
                }
            }
        }
    }

    final static int[] spaces = { 6, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 6 };
    final static int[] length = { 6, 7, 8, 9, 10, 11, 12, 11, 10, 9, 8, 7, 6 };
    final static int[] startx = { 6, 5, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0 };

    // Display a candidate in visual form
    void print() {
        for (int y = 12; y >= 0; y--) {
            for (int i = 0; i < spaces[y]; i++) {
                System.out.print(" ");
            }
            int x = startx[y];
            for (int l = 0; l <= length[y]; l++) {
                System.out.print(dna[x][y]);
                System.out.print(" ");
                x += 1;
            }
            System.out.println();
        }
    }

    // Randomly pick DNA from two candidates and produce an offspring
    Candidate breedWith(Candidate candidate) {
        Candidate c = new Candidate();
        c.dna = new char[14][14];
        for (int y = 12; y >= 0; y--) {
            if (r.nextBoolean()) { // Take the row from us
                for (int x = 12; x >= 0; x--) {
                    c.dna[x][y] = dna[x][y];
                }
            } else { // Take the row from the other candidate
                for (int x = 12; x >= 0; x--) {
                    c.dna[x][y] = candidate.dna[x][y];
                }
            }
        }
        return c;
    }

    // Mutate this candidate a bit
    Candidate mutate() {
        for (int y = 12; y >= 0; y--) {
            for (int x = 12; x >= 0; x--) {
                if (r.nextInt(10) == 1) { // 10% chance of mutation
                    dna[x][y] = characterSet[r.nextInt(26)];
                }
            }
        }
        return this;
    }

    int test(String regex, String s) {
//        System.out.println("Test: " + s + " using " + regex);
        if (s.matches(regex))
            return 1;
        else
            return 0;
    }


    // "A" slices cut left to right through the hexagonal matrix
    final int[] ax = { 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6 };
    final int[] ay = { 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
    String a(int slice) {
        StringBuilder s = new StringBuilder(length[slice]);
        int x = ax[slice];
        int y = ay[slice];
        for (int i = 0; i <= length[slice]; i++) {
            s.append(dna[x][y]);
            x += 1;
            // y unchanged
        }
        return s.toString();
    }

    // "B" slices cut from the top right towards the lower left
    final int[] bx = { 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
    final int[] by = { 6, 7, 8, 9, 10, 11, 12, 12, 12, 12, 12, 12, 12 };
    String b(int slice) {
        StringBuilder s = new StringBuilder(length[slice]);
        int x = bx[slice];
        int y = by[slice];
        for (int i = 0; i <= length[slice]; i++) {
            s.append(dna[x][y]);
            // x unchanged
            y -= 1;
        }
        return s.toString();
    }

    // "C" slices cut from the bottom right towards the upper left
    final int[] cx = { 6, 7, 8, 9, 10, 11, 12, 12, 12, 12, 12, 12, 12 };
    final int[] cy = { 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6 };
    String c(int slice) {
        StringBuilder s = new StringBuilder(length[slice]);
        int x = cx[slice];
        int y = cy[slice];
        for (int i = 0; i <= length[slice]; i++) {
            s.append(dna[x][y]);
            x -= 1;
            y += 1;
        }
        return s.toString();
    }

    // This function evaluates each regular expression against the appropriate slice
    public int evaluate() {
        int score = 0;

        // Rows
        score += test(".*H.*H.*",                       a(0));
        score += test("(DI|NS|TH|OM)*",                 a(1));
        score += test("F.*[AO].*[AO].*",                a(2));
        score += test("(O|RHH|MM)*",                    a(3));
        score += test(".*",                             a(4));
        score += test("C*MC(CCC|MM)*",                  a(5));
        score += test("[^C]*[^R]*III.*",                a(6));
        score += test("(...?)\\1*",                     a(7));
        score += test("([^X]|XCC)*",                    a(8));
        score += test("(RR|HHH)*.?",                    a(9));
        score += test("N.*X.X.X.*E",                    a(10));
        score += test("R*D*M*",                         a(11));
        score += test(".(C|HH)*",                       a(12));

        // Top-down
        score += test(".*SE.*UE.*",                     b(0));
        score += test(".*LR.*RL.*",                     b(1));
        score += test(".*OXR.*",                        b(2));
        score += test("([^EMC]|EM)*",                   b(3));
        score += test("(HHX|[^HX])*",                   b(4));
        score += test(".*PRR.*DDC.*",                   b(5));
        score += test(".+",                             b(6));
        score += test("[AM]*CM(RC)*R?",                 b(7));
        score += test("([^MC]|MM|CC)*",                 b(8));
        score += test("(E|CR|MN)*",                     b(9));
        score += test("P+(..)\\1.*",                    b(10));
        score += test("[CHMNOR]*I[CHMNOR]*",            b(11));
        score += test("(ND|ET|IN)[^X]*",                b(12));

        // Bottom-up
        score += test(".*G.*V.*H.*",                    c(0));
        score += test("[CR]*",                          c(1));
        score += test(".+XEXM*",                        c(2));
        score += test(".DD.*CCM.*",                     c(3));
        score += test(".*XHCR.*X.*",                    c(4));
        score += test(".*(.)(.)(.)(.)\\4\\3\\2\\1.*",   c(5));
        score += test(".*(IN|SE|HI)",                   c(6));
        score += test("[^C]*MMM[^C]*",                  c(7));
        score += test(".+(.)C\\1X\\1.*",                c(8));
        score += test("[CEIMU]*OH[AEMOR]*",             c(9));
        score += test("(RX|[^R])*",                     c(10));
        score += test("[^M]*M[^M]*",                    c(11));
        score += test("(S|MM|HHH)*",                    c(12));

        return score;
    }
}
