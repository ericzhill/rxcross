package rxcross;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
    void start(String[] args) throws Exception {
        Random r = new Random();
        final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

        List<Kingdom> kingdoms = new ArrayList<>();
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));
        kingdoms.add(new Kingdom(r.nextInt(9000) + 1000));

        List<Thread> kingdomThreads = new ArrayList<>();
        for (Kingdom kingdom : kingdoms) {
            Thread runner = new Thread(kingdom);
            runner.start();
            kingdomThreads.add(runner);
        }

        int bestScore = 0;
        int genocide = 6;
        while (true) {
            Thread.sleep(10000);
            if (genocide-- == 0) {
                System.out.println("Committing genocide on an entire kingdom");

                // Kill off a random kingdom
                int markedForDeath = r.nextInt(kingdoms.size());
                kingdoms.get(markedForDeath).terminated = true;
                kingdoms.remove(markedForDeath);
                kingdomThreads.remove(markedForDeath);

                Kingdom newKingdom = new Kingdom(r.nextInt(9000) + 1000);
                kingdoms.add(newKingdom);
                Thread runner = new Thread(newKingdom);
                runner.start();
                kingdomThreads.add(runner);

                genocide = 6;
            }
            for (Kingdom kingdom : kingdoms) {
                kingdom.marryInto(kingdoms.get(r.nextInt(kingdoms.size())));
                int kingdomFitness = kingdom.goldMedal;
                if (kingdomFitness >= bestScore) {
                    bestScore = kingdomFitness;
                    kingdom.gold.print();
                    System.out.println(Integer.toString(kingdom.id) + " has score " + Integer.toString(kingdomFitness));
                }
            }
        }

    }

    public static void main(String[] args) {
        Main main = new Main();
        try {
            main.start(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

}
